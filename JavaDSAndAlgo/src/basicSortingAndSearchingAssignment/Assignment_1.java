package basicSortingAndSearchingAssignment;

import java.util.Scanner;

class Employee{
	int employeeNumber;
	String firstName;
	String lastName;
	String emailID;
	public Employee(int empNumber, String f, String l, String emailid){
		this.employeeNumber = empNumber;
		this.firstName = f;
		this.lastName = l;
		this.emailID = emailid;
	}
	
	public void Display(){
		System.out.println("Empplyee Number is "+employeeNumber);
		System.out.println(firstName);
		System.out.println(lastName);
		System.out.println(emailID);
		System.out.println();
	}
}

class Assignment_1 {

	public static void main(String[] args) {
		Scanner n = new Scanner(System.in);
		System.out.println("Enter size of array");
		int size = n.nextInt();
		System.out.println();
		
		int empNum;
		String fn;
		String ln;
		String em;
		Employee[] emparray = new Employee[size];
		for(int i=0; i<emparray.length; i++){
			System.out.println("Enter Employee Number");
			empNum = n.nextInt();
			n.nextLine();
			System.out.println("Enter First Name");
			fn = n.nextLine();
			
			System.out.println("Enter Last Name");
			ln = n.nextLine();
			
			System.out.println("Enter email id");
			em = n.nextLine();
			
			emparray[i] = new Employee(empNum, fn, ln, em);
		}
		System.out.println();
		
		System.out.println("Insertion Sorting...");
		//Insertion Sort.
		Employee current;
		int j;
		for(int i=0; i<emparray.length; i++){
			current = emparray[i];
			j = i-1;
			while(j>=0 && emparray[j].employeeNumber > current.employeeNumber){
				emparray[j+1] = emparray[j];
				j--;
			}
			emparray[j+1] = current;
		}
		
		//Print Employee Array
		for(int i = 0; i<emparray.length; i++){
			emparray[i].Display();
		}
		
	}

}
