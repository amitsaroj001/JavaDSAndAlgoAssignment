package basicSortingAndSearchingAssignment;

import java.util.Scanner;

public class Assignment_2 {

	public static boolean hasDuplicateChars(String s){
		char[] sc = s.toCharArray();
		
		//Insertion Sort
		char current;
		int j;
		for(int i=0; i<sc.length;i++){
			current = sc[i];
			j = i-1;
			while(j>=0 && sc[j]>current ){
				sc[j+1] = sc[j];
				j--;
			}
			sc[j+1] = current;
		}
		
		for(int i=0; i<sc.length-1; i++){
			if(sc[i] == sc[i+1])
				return true;
			else
				return false;
		}
		return false;
	}
	
	public static void main(String[] args) {
		 Scanner n = new Scanner(System.in);
		 
		 System.out.println("Enter the word");
		 String s = n.nextLine();
		 
		 System.out.println(hasDuplicateChars(s));
		 n.close();
		
	}

}
